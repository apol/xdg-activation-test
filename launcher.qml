import QtQuick 2.4
import QtQuick.Window 2.4
import QtQuick.Layouts 2.4
import QtQuick.Controls 2.3

Window {
    id: win
    width: 200
    height: 400
    visible: true
    property QtObject testObject
    property alias label: textItem.text

    ColumnLayout {
        anchors.fill: parent
        Label {
            id: textItem
            text: 'potato'
        }

        Button {
            text: 'Request token'
            onClicked: win.testObject.requestToken()
        }
        Button {
            text: 'Launch Subject'
            onClicked: win.testObject.launchSubject()
        }
        TextField {
            id: subjectToActivate
            text: "0"
        }
        Button {
            text: 'Send token'
            onClicked: win.testObject.sendToken(subjectToActivate.text)
        }
        RowLayout {
            Button {
                text: 'Activate Subject'
                onClicked: win.testObject.activateSubject(subjectToActivate.text)
            }
        }
    }
}
