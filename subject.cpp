#include <QObject>
#include <QGuiApplication>
#include <QWindow>
#include <QQuickWindow>
#include <QDBusConnection>
#include <QQmlApplicationEngine>
#include <private/qwaylandwindow_p.h>

class TestObject : public QObject
{
Q_OBJECT
public:
    TestObject() {
        auto bus = QDBusConnection::sessionBus();
        qDebug("a");
        const bool b = bus.registerService(QLatin1String("org.freedesktop.TestSubject"));
        Q_ASSERT(b);
        if (!bus.registerObject(QLatin1String("/TestSubject"), "org.freedesktop.TestObject", this, QDBusConnection::ExportAllSlots)) {
            qWarning() << "could not register";
            QCoreApplication::exit(1);
            return;
        }

        connect(&engine, &QQmlApplicationEngine::objectCreated, this, [this] (QObject* object) {
            auto window = dynamic_cast<QQuickWindow*>(object);
            Q_ASSERT(window);
            m_windows << window;
        });
    }

    ~TestObject() {
        qDeleteAll(m_windows);
    }

public Q_SLOTS:
    void addWindow() {
        engine.loadData("import QtQuick 2.4\nimport QtQuick.Window 2.4\n"
                        "Window { width: 200; height: 200; visible: true; Rectangle { color: 'red'; anchors.fill: parent } }");
    }

    void requestActivation(int at) {
        m_windows[at]->requestActivate();
    }

    void raise(int at) {
        m_windows[at]->raise();
    }

    void setActivationToken(int at, const QString &token) {
        auto window = m_windows[at];
        auto wlWindow = dynamic_cast<QtWaylandClient::QWaylandWindow *>(window->handle());
        Q_ASSERT(wlWindow);

        wlWindow->setXdgActivationToken(token);
    }

private:
    QQmlApplicationEngine engine;
    QVector<QWindow*> m_windows;
};

int main(int argc, char** argv)
{
    QGuiApplication app(argc, argv);
    QCoreApplication::setOrganizationDomain("freedesktop.org");

    TestObject o;
    o.addWindow();

    app.exec();
}

#include "subject.moc"
