#include <QGuiApplication>
#include <QWindow>
#include <QProcess>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QQuickItem>
#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusPendingCall>

#include <private/qwaylandwindow_p.h>

class Test : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString token READ token NOTIFY tokenChanged)
public:
    Test() {
        connect(&engine, &QQmlApplicationEngine::objectCreated, this, [this] (QObject* object) {
            auto window = dynamic_cast<QQuickWindow*>(object);
            Q_ASSERT(window);
            Q_ASSERT(!m_window);
            m_window = window;

            auto wlWindow = dynamic_cast<QtWaylandClient::QWaylandWindow *>(m_window->handle());
            Q_ASSERT(wlWindow);
            QObject::connect(wlWindow, &QtWaylandClient::QWaylandWindow::xdgActivationTokenCreated, this, [this] (const QString &_token) {
                m_token = _token;
                qDebug() << "token received!" << m_token;
                m_window->setProperty("label", m_token);
            });
        });


        engine.setInitialProperties({
            { "label", "<none>" },
            { "testObject", QVariant::fromValue<QObject*>(this)}
        });

        engine.load(QUrl(QStringLiteral("qrc:/launcher.qml")));
    }

    QString token() const { return m_token; }

public Q_SLOTS:
    void requestToken() {
        auto wlWindow = dynamic_cast<QtWaylandClient::QWaylandWindow *>(m_window->handle());
        Q_ASSERT(wlWindow);

        qDebug() << "request!";
        wlWindow->requestXdgActivationToken(0);
    }

    void launchSubject() {
        QProcess::startDetached("subject", {});
    }

    void sendToken(int which) {
        auto msg = QDBusMessage::createMethodCall(
            QStringLiteral("org.freedesktop.TestSubject"),
            QStringLiteral("/TestSubject"),
            QStringLiteral("org.freedesktop.TestObject"),
            QStringLiteral("setActivationToken"));
        msg.setArguments({which, m_token});
        QDBusConnection::sessionBus().asyncCall(msg);
    }

    void activateSubject(int which) {
         auto msg = QDBusMessage::createMethodCall(
            QStringLiteral("org.freedesktop.TestSubject"),
            QStringLiteral("/TestSubject"),
            QStringLiteral("org.freedesktop.TestObject"),
            QStringLiteral("requestActivation"));
        msg.setArguments({which});
        QDBusConnection::sessionBus().asyncCall(msg);

    }

Q_SIGNALS:
    void tokenChanged(const QString &token);

private:
    QString m_token;
    QQuickWindow* m_window = nullptr;
    QQmlApplicationEngine engine;
};

int main(int argc, char** argv)
{
    QGuiApplication app(argc, argv);

    Test o;
    return app.exec();
}

#include "launcher.moc"
